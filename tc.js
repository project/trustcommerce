$(document).ready(function(){
  // disallow anything but numbers 
  $('#edit-submit').attr('disabled','disabled');
    $('#edit-cardnumber').keypress(function(e){
    //if the letter is not digit then display error and don't type anything
      if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57)) {
        //display error message
        $("#error-cardnumber").html("Digits Only").show().fadeOut("slow");
        return false;
      }
    });
    // disallow anything but numbers
    $('#edit-cvv').keypress(function(e){
    //if the letter is not digit then display error and don't type anything
      if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57)) {
        //display error message
        $("#error-checkcvv").html("Digits Only").show().fadeOut("slow");
        return false;
      }
    });
    // disable the submit button until the length of
    // the cardnumber is > 13
    $('form').keyup(function(event){
      var activeform = this.form;
      if ($('#edit-cardnumber').val().length > 13) {
        $('#edit-submit').removeAttr('disabled');
      }else{
        $('#edit-submit').attr('disabled','disabled');
      }
    });
    // disable the submit button after it is pressed
    $('#trustcommerce-form').submit(function() {
      $('#edit-submit').attr('disabled','disabled');
    });
});
