Payment Module  : TrustCommerce
Original Author : Dave Augustus 
Settings        : > administer > store > settings > trustcommerce 

********************************************************************
DESCRIPTION:

Accept payments using TrustCommerce. This module uses the TCLink API
to submit transactions to the payment gateway. 

Supports Trustcommerce's Citadel feature set:
  This allows you to store the billingid instead of CC or ACH 
  information on your server, providing for a more secure 
  storage mechanism for you and your customers.

Supports Autopay: (heavy development- caution!!!!)
  Integrates with ec_autopay and ec_recurring to provide payment
  methods for subscriptions. 

********************************************************************
SPECIAL NOTES:

You must have the tclink.so file available to php and installed on
your server. You can obtain this from TrustCommerce.

This module borrows code from Matt Westgate's Authorize.net module so
a special thanks goes out to him: Thanks Matt!

********************************************************************


